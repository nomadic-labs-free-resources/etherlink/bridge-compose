#! /bin/sh
set -e

clean_directories() {
  echo "Cleaning directories..."
  
  find "/home/tezos/.tezos-client" -type f -not -name '.gitkeep' -delete
  find "/home/tezos/.tezos-client" -type d -empty -delete

  find "/home/tezos/.tezos-smart-rollup-node" -type f -not -name '.gitkeep' -not -name 'config.json' -delete
  find "/home/tezos/.tezos-smart-rollup-node" -type d -empty -delete

  find "/home/tezos/artifacts" -type f -not -name '.gitkeep' -delete
  find "/home/tezos/artifacts" -type d -empty -delete
  
  echo "Cleaning completed."
}

init() {
  # Init the client
  octez-client --endpoint "${TEZOS_ENDPOINT}" config init
  # data/tezos-client/config should look like:
  # { "base_dir": "/home/tezos/.tezos-client",
  #   "endpoint": "https://ghostnet.tezosrpc.midl.dev/ak-1gzw047cj1v8qd",
  #   "web_port": 8080, "confirmations": 0 }

  # Initialize smart rollup node configuration
  # octez-smart-rollup-node init observer \
  #   config for "${ETHERLINK_SMART_ROLLUP_ADDRESS}" \
  #   with operators \
  #   --history-mode archive \
  #   --pre-images-endpoint "${ETHERLINK_PRE_IMAGES_ENDPOINT}"
  # data/tezos-smart-rollup-node/config should look like:
  # { "smart-rollup-address": "sr18wx6ezkeRjt1SZSeZ2UQzQN3Uc3YLMLqg",
  #   "smart-rollup-node-operator": {}, "fee-parameters": {}, "mode": "observer",
  #   "pre-images-endpoint":
  #     "https://snapshots.eu.tzinit.org/etherlink-ghostnet/wasm_2_0_0",
  #   "history-mode": "archive" }

  # Import snapshot
  wget -O /home/tezos/artifacts/eth.archive "${ETHERLINK_SNAPSHOT_URL}"
  octez-smart-rollup-node snapshot import ./artifacts/eth.archive
  rm -rf /home/tezos/artifacts/eth.archive

  # 4. Download and extract the WASM archive
  # wget -O /home/tezos/artifacts/wasm.tar.gz "${ETHERLINK_WASM_ARCHIVE_URL}"
  # tar zvxf /home/tezos/artifacts/wasm.tar.gz -C /home/tezos/.tezos-smart-rollup-node
  # rm -rf /home/tezos/artifacts/wasm.tar.gz
}

if [ "$OCTEZ_CLEAN" = "true" ]; then
  clean_directories
  init
fi

# Run the node
octez-smart-rollup-node --base-dir /home/tezos/.tezos-client run \
  --data-dir /home/tezos/.tezos-smart-rollup-node \
  --rpc-addr 0.0.0.0 \
  --rpc-port 5000
