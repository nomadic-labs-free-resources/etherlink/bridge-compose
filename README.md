# Bridge Compose

This setup is designed for deploying an Etherlink smart rollup node observer and EVM proxy using Docker Compose.

## Get started

### Configure

Configure your environment variables in the `.env.node` and `env.rpc`.

__important__: Ensure to set the `TEZOS_ENDPOINT` variable in `.env.node` to point to your Tezos node in archive mode.

### Launch the services

To launch the Etherlink smart rollup node observer and EVM proxy, run:

```bash
docker compose up
```

### Accessing the services

The rollup node can be accessed at `http:localhost:8733`.
The EVM Proxy can be accessed at  at `http://localhost:8545`.
